import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

void main(){
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _State createState() => new _State();
}

//State is information of the application that can change over time or when some actions are taken.
class _State extends State<MyApp>{

  String _value = '';
  String _value2 = '';

  final TextEditingController _textEditingControllerStart = new TextEditingController();
  final TextEditingController _textEditingControllerEnd = new TextEditingController();

  var data;

@override
  void initState() {
    _getThingsOnStartup().then((value){
      print('Async done   ');
      var now = DateTime.now();
      var month = now.month.toString().padLeft(2, '0');
      var day = now.day.toString().padLeft(2, '0');
      _value = '$day.$month.${now.year}';
      print(_value);
      _textEditingControllerStart.text = _value;
      _textEditingControllerEnd.text = _value;

    });
    super.initState();
  }

Future _getThingsOnStartup() async {
    await Future.delayed(Duration(seconds: 2));
  }

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
    context: context,
    initialDate: DateTime.now().add(Duration(hours: 1)),
    firstDate: DateTime.now(),
    lastDate: DateTime(2101));
    if(picked != null) setState(() => _value = picked.day.toString() +'.'+picked.month.toString() +'.'+picked.year.toString() );
    _textEditingControllerStart.text = _value;
  }

  Future _selectDate2() async {
    DateTime picked = await showDatePicker(
    context: context,
    initialDate: DateTime.now().add(Duration(hours: 1)),
    firstDate: DateTime.now(),
    lastDate: DateTime(2101));    
    if(picked != null) setState(() => _value2 = picked.day.toString() +'.'+picked.month.toString() +'.'+picked.year.toString() );
    _textEditingControllerEnd.text = _value2;  
  }

  Future _raporX() async {
    print("rapor servisi cagiriliyor");
      print(_textEditingControllerStart.text);
      print(_textEditingControllerEnd.text);
     // http://159.146.30.92:81/donerci/api/values?startDate=04.08.2020&endDate=04.08.2020
  }

Future<List<Sonuc>> _rapor() async {
    print("rapor servisi cagiriliyor");
      print(_textEditingControllerStart.text);
      print(_textEditingControllerEnd.text);
    var response = await http.get(
      Uri.encodeFull("http://159.146.30.92:81/donerci/api/values?startDate=04.08.2020&endDate=04.08.2020"),
    );
/*
var datad = await http.get("https://jsonplaceholder.typicode.com/users");
    var jsonData = json.decode(datad.body);
    for(var u in jsonData){
      print(_textEditingControllerStart.text);
    }
*/

    final jsonDatax  = json.decode(response.body.replaceAll("\"\"","\""));
    List<Sonuc> sonucs =[];
    //print(jsonDatax);
    
    String ss= "[{\"TARIH\":\"0001-01-01T00:00:00\",\"KOD\":\"0103\",\"ACIKLAMA\":\" GOBIT 100GR \",\"YEMEK_KODU\":\"01\",\"YEMEK_ADI\":\"DONERLER\",\"MIKTAR\":\"      137.00\",\"SFIY\":\"     1644.00\",\"ISKONTO\":\"        0.00\",\"NET_TUTAR\":\"     1644.00\"}, {\"TARIH\":\"0001-01-01T00:00:00\",\"KOD\":\"0103\",\"ACIKLAMA\":\" GOBIT 100GR \",\"YEMEK_KODU\":\"01\",\"YEMEK_ADI\":\"DONERLER\",\"MIKTAR\":\"      137.00\",\"SFIY\":\"     1644.00\",\"ISKONTO\":\"        0.00\",\"NET_TUTAR\":\"     1644.00\"}]"; 
    ss = response.body.replaceAll("\"\"","\"");
    var dd = json.decode(ss);
    var cc = json.decode(dd);

    for(var u in cc)
    {
    print(u["ACIKLAMA"]);
    Sonuc sonuc= Sonuc(KOD:u["KOD"],ACIKLAMA:u["ACIKLAMA"],
                       YEMEK_KODU: u["YEMEK_KODU"],YEMEK_ADI: u["YEMEK_ADI"],
                       MIKTAR: u["MIKTAR"],SFIY: u["SFIY"],
                       ISKONTO: u["ISKONTO"],NET_TUTAR: u["NET_TUTAR"]);
      sonucs.add(sonuc);
    }
    print( "data count : ");
    print( sonucs.length);
    
    return sonucs;
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Dashboard'),
      ),
      //hit Ctrl+space in intellij to know what are the options you can use in flutter widgets
      body: 
      new Container(
        padding: new EdgeInsets.all(10.0),
          child: new Column(
            children: <Widget>[
                 new TextField(
                  onTap: _selectDate,
                  controller: _textEditingControllerStart,
                  decoration: InputDecoration(
                  labelText: "Baslangıç tarihi",
                  icon: Icon(Icons.calendar_today),
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
              ),
            ),
            ),
              new Text("  "),
              new TextField(
                  onTap: _selectDate2,
                  controller: _textEditingControllerEnd,
                  decoration: InputDecoration(
                  labelText: "Bitiş Tarihi",
                  icon: Icon(Icons.calendar_today),
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
              ),
            ),
            ),
            /*new Row(
              children: <Widget>[
              //new Text(_value),
              //new Text("  "),
              new RaisedButton(onPressed: _selectDate, child: new Text('Baş. tarihi'),
              color: Colors.red[400], 
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)) ,),
                ]
                ),
              new Row(
                children: <Widget>[
              new Text(_value2),
              new Text("  "),
              new RaisedButton( onPressed: _selectDate2, child: new Text('Bit. tarihi'),
               color: Colors.red[400],
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)) ,),
                ]
                ),
              */
              new Row(
                children: <Widget>[
              new Text("  "),
              new RaisedButton( onPressed: _rapor, child: new Text('Rapor'),
               color: Colors.red[400],
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)) ,),
              new FutureBuilder(
                future: _rapor(),
                builder: (BuildContext context,AsyncSnapshot snapshot){
                  if (snapshot.data == null)
                  {
                    return Container(
                      child : Center(
                        child:Text("Loading..."),
                      )
                    );
                  }else
                  {

Expanded myList() {
    return Expanded(
        child: Container(
          width: double.infinity,
          height: 200,
          child: ListView.builder(
            itemBuilder: (context, position) {
              return Card(
                child: Text("reererer"),
              );
            },
            itemCount: snapshot.data.documents.length,
          ),
        )
    );
  }


/*
                  Widget servicesListview() {
                  return ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (BuildContext context, int index){
                  /*  
                  return ListTile(
                    title:Text(snapshot.data[index].ACIKLAMA
                    ),
                  );
*/
 Container(

decoration: new BoxDecoration(color: const Color(0xFFEAEAEA)),
            child: Text("ACIKLAMA")
 );



                  },
                  );

                  }
*/


                  }
                },

              ),
                ]
                ),

            ],
        ),
        
      ),
            
    );
  }

}
    class Sonuc
    {
        //final String TARIH;
        final String KOD;
        final String ACIKLAMA;
        final String YEMEK_KODU;
        final String YEMEK_ADI;
        final String MIKTAR;
        final String SFIY;
        final String ISKONTO;
        final String NET_TUTAR;
        
        Sonuc(
          {//this.TARIH,
          this.KOD,
          this.ACIKLAMA,
          this.YEMEK_KODU,
          this.YEMEK_ADI,
          this.MIKTAR,
          this.SFIY,
          this.ISKONTO,
          this.NET_TUTAR,});
     
     factory Sonuc.fromJson(Map<String, dynamic> parsedJson){
    return Sonuc(
      //TARIH : parsedJson['TARIH'],
      KOD : parsedJson ['KOD'],
      ACIKLAMA : parsedJson['ACIKLAMA'],
      YEMEK_KODU : parsedJson ['YEMEK_KODU'],      
      YEMEK_ADI : parsedJson['YEMEK_ADI'],
      MIKTAR : parsedJson ['MIKTAR'],
      SFIY : parsedJson['SFIY'],
      ISKONTO : parsedJson ['ISKONTO'],      
      NET_TUTAR : parsedJson ['NET_TUTAR'],      
    );
  }

     
    }


